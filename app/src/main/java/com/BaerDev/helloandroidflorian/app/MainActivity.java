package com.BaerDev.helloandroidflorian.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private EditText textField;
    private TextView textToShow;
    private Button nextFinish;
    private boolean close = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textToShow = (TextView)findViewById(R.id.textToShow);
        textField = (EditText)findViewById(R.id.Name);
        nextFinish = (Button)findViewById(R.id.NextButton);
        nextFinish.setEnabled(false);
        textField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                nextFinish.setEnabled(editable.length() > 0);
            }
        });
        nextFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(close)
                {
                    finish();
                }

                textToShow.setText(getString(R.string.hello_PNAME, textField.getText()));
                textField.setVisibility(View.INVISIBLE);
                nextFinish.setText(R.string.finish);
                close = true;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
